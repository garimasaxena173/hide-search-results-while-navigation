import { LightningElement, track } from 'lwc';

export default class GlobalSearch extends LightningElement {

    @track isRendered;
    // Temporary data 
    @track searchResultList = [{
        id: 1,
        dataItem: 'result 1',
    }, {
        id: 2,
        dataItem: 'result 2',
    }, 
    ];

    handleOnBlur(){
        this.searchResultList = [];
        this.isRendered = false; 
    }

    handleInputChange(){
        this.isRendered = true;
    }

}